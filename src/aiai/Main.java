package aiai;
//提出

/**
 * 童謡アイアイを表示するプログラム @author 200217AM
 *
 */
public class Main {

	/**
	 * アイアイを表示 @return
	 */

	public static String sayAiai(){
		return "アイアイ(アイアイ)";
	}

	/**
	 * 同じメッセージを複数回表示
	 * @param mgs
	 * @param count
	 * @return
	 */
	public static String repeatMessage(String msg,int count){
		String repeatMsg="";
		for(int i=0; i<count;i++){
			repeatMsg+=msg;
		}
		return repeatMsg;
	}

	/**改行する
	 * @return
	 */
	public static String newLine(){
		return "\n";
	}

	/**おさるさんだよを表示
	 * @return
	 */
	public static String sayMonkey(){
		return "おさるさんだよ";
	}

	/**おさるさんだねを表示
	 * @return
	 */
	public static String itIsMonkey(){
		return "おさるさんだね";
	}

	/**みなみのしまのを表示
	 * @return
	 */
	public static String saySouth(){
		return"みなみのしまの";
	}

	/**きのはのおうちを表示
	 * @return
	 */
	public static String sayHouse(){
		return "きのはのおうち";
	}

	/**しっぽのながいを表示
	 * @return
	 */
	public static String sayLongTail(){
		return "しっぽのながい";
	}

	/**おめめのまるいを表示
	 * @return
	 */
	public static String sayRoundEyes(){
		return "おめめのまるい";
	}

	/**アイアイを歌う処理
	 *
	 */
	public static void execute(){
		String lyric = "";
		lyric+= repeatMessage(sayAiai(), 2);

		lyric+= newLine();
		lyric+= sayMonkey();
		lyric+= newLine();
		lyric+= repeatMessage(sayAiai(), 2);
		lyric+= newLine();
		lyric+= saySouth();
		lyric+= newLine();
		lyric+= repeatMessage(sayAiai(), 4);
		lyric+= newLine();
		lyric+= sayLongTail();
		lyric+= newLine();
		lyric+= repeatMessage(sayAiai(), 2);
		lyric+= newLine();
		lyric+= sayMonkey();

		//以降、自作のメソッドを使ってアイアイを完成させてください。

		System.out.println(lyric);

	}

	public static void execute2(){

		String lyric = "";
		lyric+= repeatMessage(sayAiai(), 2);

		lyric+= newLine();
		lyric+= itIsMonkey();
		lyric+= newLine();
		lyric+= repeatMessage(sayAiai(), 2);
		lyric+= newLine();
		lyric+= sayHouse();
		lyric+= newLine();
		lyric+= repeatMessage(sayAiai(), 4);
		lyric+= newLine();
		lyric+= sayRoundEyes();
		lyric+= newLine();
		lyric+= repeatMessage(sayAiai(), 2);
		lyric+= newLine();
		lyric+= itIsMonkey();

		//以降、自作のメソッドを使ってアイアイを完成させてください。

		System.out.println(lyric);

	}

	/**メインメソッド
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("1番");

		execute();

		System.out.println("2番");
		execute2();
	}

}
